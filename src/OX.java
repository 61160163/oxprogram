
import java.util.Scanner;

class OX {

    private static void showWelcome() {
        System.out.println("Welcome to OX Game.");
    }

    private static void getBoard(String[][] board) {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
    }

    private static String choosePlay(String[][] board) {
        String choose;
        Scanner keyBoard = new Scanner(System.in);
        System.out.print("Player1 choose O or X :");
        choose = keyBoard.next();
        if (choose.equalsIgnoreCase("O") || choose.equalsIgnoreCase("x")) {
            turnPlayer(choose, board);
        } else {
            System.out.println("Enter only O or X values.");
        }
        return choose;
    }

    private static void turnPlayer(String choose, String[][] board) {
        String player1 = setPlay1(choose);
        String player2 = setPlay2(player1);
        boolean stop = false;
        int turn = 0;
        do {
            turn++;
            if (turn % 2 == 1) {
                stop = functionPlay(player1, turn, stop, board);
            } else {
                stop = functionPlay(player2, turn, stop, board);
            }
            showDraw(turn, stop);
        } while (!stop);
    }

    private static String setPlay1(String chooseOX) {
        if (chooseOX.equalsIgnoreCase("o")) {
            return "O";
        } else {
            return "X";
        }
    }

    private static String setPlay2(String chooseOX) {
        if (chooseOX.equalsIgnoreCase("o")) {
            return "X";
        } else {
            return "O";
        }
    }

    private static boolean functionPlay(String player, int turn, boolean stop, String[][] board) {
        showTurn(player);
        inputRowCol(player, turn, board);
        getBoard(board);
        stop = checkWin(board);
        showWin(player, stop);
        return stop;
    }

    private static void showTurn(String player) {
        System.out.println("Turn " + player);
    }

    private static void inputRowCol(String player, int turn, String[][] board) {
        Scanner keyBoard = new Scanner(System.in);
        boolean checkType1 = true, checkType2 = true;
        String inputRow, inputCol;
        do {
            System.out.print("Input row : ");
            checkType1 = checkInput(inputRow = keyBoard.next());
        } while (checkType1);
        do {
            System.out.print("Input column : ");
            checkType2 = checkInput(inputCol = keyBoard.next());
        } while (checkType2);
        inputOX(player, Integer.parseInt(inputRow), Integer.parseInt(inputCol), turn, board);
    }

    private static void inputOX(String player, int row, int column, int turn, String[][] board) {
        if (board[row][column].equals("-")) {
            board[row][column] = player;
        } else {
            turn--;
            System.out.println("This row and column has already been used.");
        }
    }

    private static boolean checkInput(String insert) {
        if (insert.equals("1") || insert.equals("2") || insert.equals("3")) {
            return false;
        } else {
            System.out.println("Please enter only numeric values between 1 and 3. ");
            return true;
        }
    }

    private static void showDraw(int turn, boolean stop) {
        if ((turn == 9) && (!stop)) {
            stop = true;
            System.out.println("Draw!!!");
        }
    }

    private static boolean checkWin(String[][] board) {
        boolean stop1, stop2, stop3, stop4;
        stop1 = checkDiadonalLeft(board);
        stop2 = checkDiadonalRight(board);
        for (int num = 1; num < 4; num++) {
            stop3 = checkRow(num, board);
            stop4 = checkColumn(num, board);
            if (stop1 == true || stop2 == true || stop3 == true || stop4 == true) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiadonalLeft(String[][] board) {
        return board[1][1].equals(board[2][2]) && board[1][1].equals(board[3][3]) && !board[1][1].equals("-");
    }

    private static boolean checkDiadonalRight(String[][] board) {
        return board[1][3].equals(board[2][2]) && board[1][3].equals(board[3][1]) && !board[1][3].equals("-");
    }

    private static boolean checkColumn(int col, String[][] board) {
        return board[1][col].equals(board[2][col]) && board[1][col].equals(board[3][col]) && !board[1][col].equals("-");
    }

    private static boolean checkRow(int row, String[][] board) {
        return board[row][1].equals(board[row][2]) && board[row][1].equals(board[row][3]) && !board[row][1].equals("-");
    }

    private static void showWin(String player, boolean gameOver) {
        if (gameOver == true) {
            System.out.println(player + " Win");
        }
    }

    private static void thankYou() {
        System.out.println("Thank you for playing our game.");
    }

    public static void main(String[] args) {
        showWelcome();
        String[][] board = new String[][]{{" ", "1", "2", "3"}, {"1", "-", "-", "-"}, {"2", "-", "-", "-"}, {"3", "-", "-", "-"}};
        getBoard(board);
        String choose;
        do {
            choose = choosePlay(board);
        } while (!(choose.equalsIgnoreCase("o")) && !(choose.equalsIgnoreCase("x")));
        thankYou();
    }

}
