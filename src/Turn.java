
public class Turn{

    int turn;

    public Turn() {
        turn = 0;
    }

    public void showTurn(String player) {
        System.out.println("Turn " + player);
    }

    public void turnAdd() {
        turn++;
    }

    public void turnMinus() {
        turn--;
    }

    public int getTurn() {
        return turn;
    }

}
