
import java.util.Scanner;

public class OX3 {

    private final static String[][] board = new String[][]{{" ", "1", "2", "3"}, {"1", "-", "-", "-"}, {"2", "-", "-", "-"}, {"3", "-", "-", "-"}};
    private final static Scanner keyBoard = new Scanner(System.in);
    private static boolean stop;
    private static boolean checkType1 = true;
    private static boolean checkType2 = true;
    private static String choose;
    private static String inputRow;
    private static String inputCol;
    private static String player1;
    private static String player2;
    private static int turn;

    private static void showWelcome() {
        System.out.println("Welcome to OX Game.");
    }

    private static void getBoard() {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
    }

    private static void choosePlay() {
        System.out.print("Player1 choose O or X :");
        choose = keyBoard.next();
        if (choose.equalsIgnoreCase("O") || choose.equalsIgnoreCase("x")) {
            turnPlayer(choose);
        } else {
            System.out.println("Enter only O or X values.");
        }
    }

    private static void checkWin() {
        int countDiadonalLeft = 0, countDiadonalRight = 0;
        for (int row = 1; row < 4; row++) {
            int countCol = 0, countRow = 0;
            countDiadonalLeft += checkDiadonalLeft(row);
            countDiadonalRight += checkDiadonalRight(row);
            for (int col = 1; col < 4; col++) {
                countCol += checkColumn(row, col);
                countRow += checkRow(row, col);
                winColumn(countCol, row);
                winRow(countRow, row);
            }
        }
        winDiadonalLeft(countDiadonalLeft);
        winDiadonalRight(countDiadonalRight);
    }

    private static void showDraw() {
        if ((turn == 9) && (!stop)) {
            stop = true;
            System.out.println("Draw!!!");
        }
    }

    private static int checkDiadonalLeft(int row) {
        if ((board[1][1].equals(board[row][row])) && (!board[1][1].equals("-"))) {
            return 1;
        }
        return 0;
    }

    private static void winDiadonalLeft(int number) {
        if (number == 3) {
            System.out.println(board[1][1] + " Win");
            stop = true;
        }
    }

    private static int checkDiadonalRight(int row) {
        if ((board[1][3].equals(board[row][4 - row])) && (!board[1][3].equals("-"))) {
            return 1;
        }
        return 0;
    }

    private static void winDiadonalRight(int number) {
        if (number == 3) {
            System.out.println(board[1][3] + " Win");
            stop = true;
        }
    }

    private static int checkColumn(int row, int col) {
        if ((board[1][row].equals(board[col][row])) && (!board[1][row].equals("-"))) {
            return 1;
        }
        return 0;
    }

    private static void winColumn(int number, int row) {
        if (number == 3) {
            System.out.println(board[1][row] + " Win");
            stop = true;
        }
    }

    private static int checkRow(int row, int col) {
        if ((board[row][1].equals(board[row][col])) && (!board[row][1].equals("-"))) {
            return 1;
        }
        return 0;
    }

    private static void winRow(int number, int row) {
        if (number == 3) {
            System.out.println(board[row][1] + " Win");
            stop = true;
        }
    }

    private static void turnPlayer(String choose) {
        setPlay(choose);
        do {
            turn++;
            if (turn % 2 == 1) {
                functionPlay(player1);
            } else {
                functionPlay(player2);
            }
            showDraw();
        } while (!stop);
    }

    private static void functionPlay(String player) {
        showTurn(player);
        inputRowCol();
        inputOX(player, Integer.parseInt(inputRow), Integer.parseInt(inputCol));
        getBoard();
        checkWin();
    }

    private static void setPlay(String chooseOX) {
        if (chooseOX.equalsIgnoreCase("o")) {
            player1 = "O";
            player2 = "X";
        } else {
            player1 = "X";
            player2 = "O";
        }
    }

    private static void inputOX(String player, int row, int column) {
        if (board[row][column].equals("-")) {
            board[row][column] = player;
        } else {
            turn--;
            System.out.println("This row and column has already been used.");
        }
    }

    private static void showTurn(String player) {
        System.out.println("Turn " + player);
    }

    private static boolean checkInput(String insert) {
        if (insert.equals("1") || insert.equals("2") || insert.equals("3")) {
            return false;
        } else {
            System.out.println("Please enter only numeric values between 1 and 3. ");
            return true;
        }
    }

    private static void inputRowCol() {
        do {
            System.out.print("Input row : ");
            checkType1 = checkInput(inputRow = keyBoard.next());
        } while (checkType1);
        do {
            System.out.print("Input column : ");
            checkType2 = checkInput(inputCol = keyBoard.next());
        } while (checkType2);
    }

    private static void thankYou() {
        System.out.println("Thank you for playing our game.");
    }

    public static void main(String[] args) {
        showWelcome();
        getBoard();
        do {
            choosePlay();
        } while (!(choose.equalsIgnoreCase("o")) && !(choose.equalsIgnoreCase("x")));
        thankYou();
    }
}
