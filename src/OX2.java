
import java.util.Scanner;

public class OX2 {

    private final static String[][] board = new String[][]{{" ", "1", "2", "3"}, {"1", "-", "-", "-"}, {"2", "-", "-", "-"}, {"3", "-", "-", "-"}};
    private final static Scanner keyBoard = new Scanner(System.in);
    private static boolean stop = false;
    private static boolean checkType1 = true;
    private static boolean checkType2 = true;
    private static String choose;
    private static String inputRow;
    private static String inputCol;
    private static String player1;
    private static String player2;
    private static int turn;

    private static void showWelcome() {
        System.out.println("Welcome to OX Game.");
    }

    private static void getBoard() {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
    }

    private static void choosePlay() {
        System.out.print("Player1 choose O or X :");
        choose = keyBoard.next();
        if (choose.equalsIgnoreCase("O") || choose.equalsIgnoreCase("x")) {
            turnPlayer(choose);
        } else {
            System.out.println("Input O or X only.");
        }
    }

    private static void turnPlayer(String choose) {
        setPlay(choose);
        do {
            turn++;
            if (turn % 2 == 1) {
                functionPlay(player1);
            } else {
                functionPlay(player2);
            }
            checkDraw(turn);
        } while (!stop);
    }

    private static void functionPlay(String player) {
        showTurn(player);
        inputRowCol();
        inputOX(player, Integer.parseInt(inputRow), Integer.parseInt(inputCol));
        checkWin();
        showWin(player, stop);
    }

    private static void checkDraw(int turn) {
        if (turn == 9 && !stop) {
            System.out.println("Draw !!!");
        }
    }

    private static void checkWin() {
        getBoard();
        boolean stop1, stop2, stop3, stop4;
        stop1 = checkDiadonalLeft();
        stop2 = checkDiadonalRight();
        for (int num = 1; num < 4; num++) {
            stop3 = checkRow(num);
            stop4 = checkColumn(num);
            if (stop1 == true || stop2 == true || stop3 == true || stop4 == true) {
                stop = true;
                break;
            }
        }
    }

    private static boolean checkDiadonalLeft() {
        return board[1][1].equals(board[2][2]) && board[1][1].equals(board[3][3]) && !board[1][1].equals("-");
    }

    private static boolean checkDiadonalRight() {
        return board[1][3].equals(board[2][2]) && board[1][3].equals(board[3][1]) && !board[1][3].equals("-");
    }

    private static boolean checkColumn(int col) {
        return board[1][col].equals(board[2][col]) && board[1][col].equals(board[3][col]) && !board[1][col].equals("-");
    }

    private static boolean checkRow(int row) {
        return board[row][1].equals(board[row][2]) && board[row][1].equals(board[row][3]) && !board[row][1].equals("-");
    }

    private static void showWin(String player, boolean gameOver) {
        if (gameOver == true) {
            System.out.println(player + " Win");
        }
    }

    private static void setPlay(String choose) {
        if (choose.equalsIgnoreCase("o")) {
            player1 = "O";
            player2 = "X";
        } else {
            player1 = "X";
            player2 = "O";
        }
    }

    private static void inputOX(String player, int row, int column) {
        if (board[row][column].equalsIgnoreCase("-")) {
            board[row][column] = player;
        } else {
            turn--;
            System.out.println("This row and column has already been used.");
        }
    }

    private static void showTurn(String player) {
        System.out.println("Turn " + player);
    }

    private static boolean checkInput(String insert) {
        if (insert.equals("1") || insert.equals("2") || insert.equals("3")) {
            return false;
        } else {
            System.out.println("Please enter only numeric values between 1 and 3. ");
            return true;
        }
    }

    private static void inputRowCol() {
        do {
            System.out.print("Input row : ");
            checkType1 = checkInput(inputRow = keyBoard.next());
        } while (checkType1);
        do {
            System.out.print("Input column : ");
            checkType2 = checkInput(inputCol = keyBoard.next());
        } while (checkType2);
    }

    private static void thankYou() {
        System.out.println("Thank you for playing our game.");
    }

    public static void main(String[] args) {
        showWelcome();
        getBoard();
        do {
            choosePlay();
        } while (!(choose.equalsIgnoreCase("o")) && !(choose.equalsIgnoreCase("x")));
        thankYou();
    }
}
