
import java.util.Scanner;

public class Board{

    Scanner keyBoard = new Scanner(System.in);
    String[][] board;
    Turn turn = new Turn();
    Player player = new Player();
    CheckWin check = new CheckWin();
    int num = 0;

    public Board() {
        board = new String[][]{{" ", "1", "2", "3"}, {"1", "-", "-", "-"}, {"2", "-", "-", "-"}, {"3", "-", "-", "-"}};
    }

    public void getBoard() {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
    }

    public String checkBoard(int row, int col) {
        return board[row][col];

    }

    public void setBoard(int row, int column, String player) {
        board[row][column] = player;
    }

    public String choosePlay() {
        String choose;
        System.out.print("Player1 choose O or X :");
        choose = keyBoard.next();
        if (choose.equalsIgnoreCase("O") || choose.equalsIgnoreCase("x")) {
            turnPlayer(choose);
        } else {
            System.out.println("Enter only O or X values.");
        }
        return choose;
    }

    public void turnPlayer(String choose) {
        player.setPlayer(choose);
        do {
            turn.turnAdd();
            if (turn.getTurn() % 2 == 1) {
                check.setStop(functionPlay(player.getPlayer1()));
                player.updateCountP1(check.stop);
            } else {
                check.setStop(functionPlay(player.getPlayer2()));
                player.updateCountP2(check.stop);
            }
            check.showDraw(turn.getTurn());
        } while (!check.getStop());
        num = player.returnCount();
    }

    public boolean functionPlay(String player) {
        turn.showTurn(player);
        inputRowCol(player);
        getBoard();
        check.checkWin();
        check.showWin(player);
        return check.getStop();
    }

    public void inputRowCol(String player) {
        boolean checkType1 = true, checkType2 = true;
        String inputRow, inputCol;
        do {
            System.out.print("Input row : ");
            checkType1 = check.checkInput(inputRow = keyBoard.next());
        } while (checkType1);
        do {
            System.out.print("Input column : ");
            checkType2 = check.checkInput(inputCol = keyBoard.next());
        } while (checkType2);
        inputOX(player, Integer.parseInt(inputRow), Integer.parseInt(inputCol));
    }

    public void inputOX(String player, int row, int col) {
        if (board[row][col].equals("-")) {
            board[row][col] = player;
        } else {
            turn.turnMinus();
            System.out.println("This row and column has already been used.");
        }
    }

    public int addCount() {
        return num;
    }
}
