
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UnitTest {

    @Test
    void testBoard() {
        Board board = new Board();
        assertEquals("-", board.checkBoard(1, 1));
    }

    @Test
    void showCheckRow() {
        Board b = new Board();
        CheckWin c = new CheckWin();
        b.inputOX("O", 1, 1);
        b.inputOX("O", 1, 2);
        b.inputOX("O", 1, 3);
        assertEquals(true, c.checkRow(1));
    }

    @Test
    void showCheckCol() {
        Board b = new Board();
        CheckWin c = new CheckWin();
        b.inputOX("X", 1, 1);
        b.inputOX("X", 2, 1);
        b.inputOX("X", 3, 1);
        assertEquals(true, c.checkColumn(3));
    }

    @Test
    void showCheckBoard() {
        Board b = new Board();
        assertEquals("-", b.checkBoard(1, 1));
    }

    @Test
    void checkShowDraw() {
        CheckWin c = new CheckWin();
        c.showDraw(9);
    }

    @Test
    void checkSetPlayer1() {
        Player p = new Player();
        p.setPlayer("O");
        assertEquals("O", p.getPlayer1());
    }

    @Test
    void checkSetPlayer2() {
        Player p = new Player();
        p.setPlayer("O");
        assertEquals("X", p.getPlayer2());
    }

    @Test
    void checkRowCol() {
        Board b = new Board();
        b.setBoard(1, 1, "O");
        assertEquals("O", b.checkBoard(1, 1));
    }

    @Test
    void checkInput() {
        CheckWin c = new CheckWin();
        c.checkInput("1");
    }

    @Test
    void setStop() {
        CheckWin c = new CheckWin();
        c.setStop(true);
        assertEquals(true, c.getStop());
    }

    @Test
    void showWin() {
        CheckWin c = new CheckWin();
        c.setStop(true);
        c.showWin("O");
    }
}
