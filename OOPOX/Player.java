
public class Player extends OXMain {

    String player1;
    String player2;
    int countP1;
    int countP2;
    int countDraw;

    public Player() {
        player1 = "O";
        player2 = "X";
        countP1 = 0;
        countP2 = 0;
        countDraw = 0;
    }

    public void setPlayer(String chooseOX) {
        if (chooseOX.equalsIgnoreCase("o")) {
            player1 = "O";
            player2 = "X";
        } else {
            player1 = "X";
            player2 = "O";
        }
    }

    public String getPlayer1() {
        return player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void updateCountP1(boolean stop) {
        if (stop == true) {
            countP1 = 1;
        }
    }

    public void updateCountP2(boolean stop) {
        if (stop == true) {
            countP2 = 1;
        }
    }

    public void updateCountDraw() {
        countDraw = 1;
    }

    public int returnCount() {
        int num = 0;
        if (countP1 == 1) {
            num = 1;
        } else if (countP2 == 1) {
            num = 2;
        } else {
            countDraw = 1;
            num = 3;
        }
        return num;
    }
}
